#include <iostream>
#include "ctime"    


int main()
{
    const time_t t = time(NULL);
    struct tm lt;
    localtime_s(&lt,&t);    
    int day = lt.tm_mday;
    

    const int N = 3;
    int array[N][N];
    int sum = 0;
    int target_row = day % N;
    
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j], "\n";
            if (i == target_row)
            {
                sum += array[i][j];
            }
            
        }
        std::cout << "\n";
    }
    std::cout << sum;
}